package com.bussanq.api.k8s.core;

import com.jfinal.render.JsonRender;
import com.jfinal.render.Render;
import com.jfinal.render.RenderFactory;

/**
 * 需要返回json错误时配置
 * @author lizh
 * @date 2019/3/18
 */
public class MyRenderFactory extends RenderFactory {
	@Override
	public Render getErrorRender(int errorCode) {
		if (errorCode == 500 || errorCode ==404) {
			return new JsonRender("error");
		}
		return super.getErrorRender(errorCode);
	}
}
