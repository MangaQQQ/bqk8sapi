package com.bussanq.api.k8s.core;

import com.bussanq.api.k8s.k8s.PodController;
import com.jfinal.config.*;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.render.RenderManager;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;
import org.apache.log4j.Logger;

/**
 * @author lizh
 * 2017
 */
public class MainConfig extends JFinalConfig {
    private static Logger logger = Logger.getLogger(MainConfig.class);
    // 使用 jfinal-undertow 时此处仅保留声明，不能有加载代码
    private static Prop p;

    // 先加载开发环境配置，再追加生产环境的少量配置覆盖掉开发环境配置
    static void loadConfig() {
        if (p == null) {
            p = PropKit.use("my_config.txt").appendIfExists("my_config_pro.txt");
        }
    }

    public void configConstant(Constants me) {
        loadConfig();
        me.setDevMode(p.getBoolean("devMode", false));
        me.setEncoding("utf-8");
        // 设置全局json错误回复
        RenderManager.me().setRenderFactory(new MyRenderFactory());
        // 支持 Controller、Interceptor、Validator 之中使用 @Inject 注入业务层，并且自动实现 AOP
        me.setInjectDependency(true);
        // 是否对超类中的属性进行注入
        // me.setInjectSuperClass(true);
    }

    public void configRoute(Routes me) {
//        me.setMappingSuperClass(true);
        me.add("/", IndexController.class);
        me.add("/api", PodController.class);
    }

    public void configPlugin(Plugins me) {
        loadConfig();
        // 配置Druid数据库连接池插件
//        HikariCpPlugin dbPlugin = new HikariCpPlugin(p.get("jdbcUrl"), p.get("user"), p.get("password").trim());
//        me.add(dbPlugin);

        // 配置ActiveRecord插件
//        ActiveRecordPlugin arp = new ActiveRecordPlugin(dbPlugin);
        // sql日志打印
//        arp.setShowSql(p.getBoolean("devMode", false));
        // 所有映射在 MappingKit 中自动化搞定
//        _MappingKit.mapping(arp);
//        me.add(arp);

        /*RedisPlugin redisPlugin = new RedisPlugin("weixin",
                "47.93.230.105",6379,2000, p.get("redis"),5);
        me.add(redisPlugin);*/

//        Cron4jPlugin cp = new Cron4jPlugin(PropKit.use("cron4j.txt"));
//        me.add(cp);
    }

    public void configInterceptor(Interceptors me) {

    }

    public void configHandler(Handlers me) {

    }

    @Override
    public void configEngine(Engine engine) {
        engine.setDevMode(p.getBoolean("devMode", false));
        Engine k8sEngine = Engine.create("k8sEngine");
        k8sEngine.setBaseTemplatePath("k8s");
        k8sEngine.setToClassPathSourceFactory();
    }

    public void onStart() {
        logger.info("初始化");
    }

    public static void main(String[] args) {
        //新的启动方法
        UndertowServer.start(MainConfig.class);
    }

}
