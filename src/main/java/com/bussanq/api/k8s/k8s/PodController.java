package com.bussanq.api.k8s.k8s;

import com.alibaba.fastjson.JSON;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Kv;

/**
 * Pod 控制器
 * @author lizh
 * @date 2019/9/14
 */
public class PodController extends Controller {

	@Inject
	PodService podService;

	public void index() {

		renderJson(podService.apply());
	}

	public void create(){
		String json = getRawData();
		PodVo pod = JSON.parseObject(json,PodVo.class);
		renderJson(Kv.by("data",podService.create(pod)));
	}

	public void getPod() {
		renderJson(Kv.by("podname",podService.pod()));
	}
}
