package com.bussanq.api.k8s.k8s;

/**
 * @author lizh
 * @date 2019/9/19
 */
public class PodVo {
	private String replicas;
	private String imagename;

	public String getReplicas() {
		return replicas;
	}

	public void setReplicas(String replicas) {
		this.replicas = replicas;
	}

	public String getImagename() {
		return imagename;
	}

	public void setImagename(String imagename) {
		this.imagename = imagename;
	}
}
