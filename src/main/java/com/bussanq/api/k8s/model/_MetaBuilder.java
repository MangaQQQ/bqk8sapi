package com.bussanq.api.k8s.model;

import com.jfinal.plugin.activerecord.generator.MetaBuilder;

import javax.sql.DataSource;

/**
 * @author lizh
 * @date 2017/12/7
 */
public class _MetaBuilder extends MetaBuilder {
	public _MetaBuilder(DataSource dataSource) {
		super(dataSource);
	}

	@Override
	protected boolean isSkipTable(String tableName) {
		return !tableName.startsWith("T_TFM_LINK_DIR_5M_FLOW_");
	}
}
